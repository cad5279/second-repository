Feature: Troubleshooting Exercise

Background:
Given I import scenarios from "features/utilities/Data Management.feature"
And I import scenarios from "features/utilities/Dragon Summoning.feature"
When I execute scenario "Data Setup"
Then I execute scenario "Data Override"

After Scenario:
Given I verify number $dragons_summoned is equal to 5

# @wip
Scenario: Troubleshooting Exercise
If I verify text $continue is equal to "FALSE"
	Then I fail step with error message "The test has failed!"
# True was spelled incorrectly
Elsif I verify text $continue is equal to "TRUE"
	# Added quantity_of_dragons assignment
    # changed to 5
	Given I assign 5 to variable "quantity_of_dragons"
	And I execute scenario "Summon Dragons"
Else I "see that nothing makes sense"
	Then I fail step with error message "The test has failed!"
Endif