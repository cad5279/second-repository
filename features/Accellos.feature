Feature: Open Accellos

Scenario: Open Accellos Login Screen

Given I "open Accellos"
	When I open "Internet Explorer" web browser
    And I navigate to "http://192.168.10.170:7480/SE2DAD/dad/dadService.jsp" in web browser
    Then I see "Accellos" in web browser
    
And I "login to Accellos"
	When I type "cdavis" in element "xPath://input[@name='USERNAME']" in web browser
    And I press keys "ENTER" in web browser
    And I type "americold" in element "xPath://input[@name='PASSWORD']" in web browser
    And I press keys "ENTER" in web browser
    

