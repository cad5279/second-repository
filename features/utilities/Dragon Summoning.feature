Feature: Dragon Summoning

# Uncommented
@wip
# Changed Sumnom to Summon
Scenario: Summon Dragons
Given I verify variable "quantity_of_dragons" is assigned
Given I assign 0 to variable "dragons_summoned"
# Removed or equal to
While I verify number $dragons_summoned is less than $quantity_of_dragons
# 0 5
# 1 5
# 2 5
# 3 5
# 4 5
	Given I "summon Dragon"
	And I execute scenario "Hello World"
    # Incremented dragons_summoned
    Then I increase variable "dragons_summoned" by 1
Endwhile

@wip
Scenario: Hello World
Given I echo "Hello World!"