Feature: CycleFoundations

Scenario: Test
Given I assign "TRUE" to variable "do_it"
And I assign 10 to variable "number"

If I verify text $do_it is equal to "TRUE"
And I verify number $number is greater than 10
And I verify number $number is less than 100
Endif

@wip
Scenario: Text If Else
Given I assign "NUGGET" to variable "do_it"
If I verify text $do_it is equal to "TRUE"
	Then I "do it!"
Elsif I verify text $do_it is equal to "FALSE"
	Then I "don't do it!"
Else I "got something I didn't expect"
Endif

@wip
Scenario: Receiving Test Case
Given I assign 1 to variable "unique_process"
Given I execute scenario "Receiving"

@wip
Scenario: Receiving
If I verify number $unique_process is equal to 1
	Then I "do a Unique Process before Receiving"
Elsif I verify number $unique_process is equal to 2
	Then I "do Unique Process 2 before Receiving"
Else I "am not doing a Unique Process"
	Then I "do this always unless Unique Process 1 or 2 happen"
EndIf
Then I echo "do Receiving"

@wip
Scenario: Variable Assignment
Given I assign 2 to variable "test"
Given I echo $test

Given I assign "Bananaphone" to variable "testing_banana"
Given I echo $testing_banana

Then I execute scenario "Open Notepad"

@wip
Scenario: Open Notepad
Given I open app "Notepad" at "C:\Windows\System32\notepad.exe" 
When I enter $testing_banana
Then I wait 5 seconds
Then I press keys CTRL+W
Then I press keys RIGHT 
Then I press keys ENTER

Given I echo $test

@wip
Scenario: Create a File in Features/Utilities, copy it to the Project Root Directory, and verify it exists
Given I create file "features/utilities/sample.csv" 
Given I copy file "features/utilities/sample.csv" to "sample.csv"
Given I verify file "sample.csv" exists

@wip
Scenario: 
Given I minimize Cycle
Then I wait 5 seconds

Given I move mouse to screen location 70 90
Given I double click current position
Then I wait 5 seconds

Given I maximize Cycle